package com.vice;

import org.jsoup.nodes.Element;

import android.graphics.Bitmap;

public class Video implements ViceItem {

	String url;
	String title;
	String content;
	
	@Override
	public String toString() {
		return "Video [url=" + url + ", title=" + title + ", content="
				+ content + "]";
	}

	public Video(Element htmlCode) {
		
	}
	
	public void setUrl(String url) {
		this.url = url;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@Override
	public String getUrl() {
		return url;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public String getContent() {
		return content;
	}

	@Override
	public ItemType getItemType() {
		return ItemType.ARTICLE;
	}

	@Override
	public Bitmap getImageBitmap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isOk() {
		// TODO Auto-generated method stub
		return false;
	}

}
