package com.vice;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.content.Context;
import android.util.Log;



public class ViceParser{
	StringBuffer stringBuffer = new StringBuffer("");
	Context c;
	
	public ViceParser (Context context) {
		c = context;
}
	
	private ArrayList<ViceItem> parseArticles(Document viceHtml)
	{
		ArrayList<ViceItem> itemList = new ArrayList<ViceItem>();
//		Log.d("PARSER", "html : " + viceHtml.data());
		Elements articles = viceHtml.select("li.story");
		
		for (Element art : articles) {
			ViceItem toPush = new Article(art);
			if (toPush.isOk())
			{
				itemList.add(toPush);
				Log.v("VICEPARSER", "Add item "+ toPush.toString());
			}
		}
		return itemList;
	}

	public ArrayList<ViceItem> getItemsFromUrl(String url) {
		// Making HTTP request
		Document d = null;
		Log.d("VICEPARSER", "HELLO WORLD with url " + url);
		try {
			d = Jsoup.connect(url).userAgent("Mozilla").get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e("VICEPARSER", "error : " + e.getMessage());
		}
		return parseArticles(d);
	}
}
