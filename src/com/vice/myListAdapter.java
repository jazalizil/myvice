package com.vice;

import java.util.ArrayList;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class myListAdapter extends ArrayAdapter<ViceItem> {
	Context c;
	ArrayList<ViceItem> items = null;

	public myListAdapter(Context context, ArrayList<ViceItem> items) {
		super(context, 0);
		this.c = context;
		this.items = items;
	}


	@Override
	public ViceItem getItem(int position) {
		Log.v("ltm","getItem("+position+")");
		return items.get(position);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		Log.v("ltm","getCount()");
		return items == null ? 0 : items.size();
	}



	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// 1. Create inflater 
		LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// 2. Get rowView from inflater
		View rowView = inflater.inflate(R.layout.row_layout, parent, false);
		ViceItem item = items.get(position);

		// 3. Get the two text view from the rowView
		TextView titleView = (TextView) rowView.findViewById(R.id.itemTitle);
		TextView contentView = (TextView) rowView.findViewById(R.id.itemContent);
		ImageView iconView = (ImageView) rowView.findViewById(R.id.itemImg);

		// 4. Set the text for textView 
		titleView.setText(item.getTitle());
		iconView.setImageBitmap(item.getImageBitmap());
		contentView.setText(item.getContent());

		// 5. retrn rowView
		return rowView;
	}

}
