package com.vice;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.util.Log;

public class Article implements ViceItem {

	String url = "";
	String title = "";
	String content = "";
	Bitmap img = null;

	private Bitmap getImageBitmap(String url) {
		Bitmap bm = null;
		try {
			URL aURL = new URL(url);
			URLConnection conn = aURL.openConnection();
			conn.connect();
			InputStream is = conn.getInputStream();
			BufferedInputStream bis = new BufferedInputStream(is);
			bm = BitmapFactory.decodeStream(bis);
			bis.close();
			is.close();
		} catch (IOException e) {
			Log.e("ARTICLE", "Error getting bitmap", e);
		}
		return bm;
	} 

	public Article(Element htmlcode) {
		Elements links = htmlcode.select("a[href]");
		Elements titles = htmlcode.select("h2");
		Elements content = htmlcode.select("p");
		Elements imgs = htmlcode.select("img");

		if (links.size() > 1)
			this.url = Html.fromHtml(links.get(1).attr("abs:href")).toString();
		if (!titles.isEmpty())
			this.title = Html.fromHtml(titles.get(0).text()).toString();
		if (!content.isEmpty())
			this.content = Html.fromHtml(content.get(0).text()).toString();
		if (!imgs.isEmpty())
			img = getImageBitmap(Html.fromHtml(imgs.get(0).attr("abs:src")).toString());
	}

	@Override
	public String toString() {
		return "Article [url=" + url + ", title=" + title + ", content="
				+ content + "]";
	}

	@Override
	public String getUrl() {
		return this.url;
	}

	@Override
	public String getTitle() {
		return this.title;
	}

	@Override
	public String getContent() {
		return this.content;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public ItemType getItemType() {
		return ItemType.ARTICLE;
	}

	@Override
	public Bitmap getImageBitmap() {
		return this.img;
	}

	@Override
	public boolean isOk() {
		return (!this.title.isEmpty() &&
			!this.url.isEmpty() &&
			!this.content.isEmpty() &&
			this.img != null);
	}

}
