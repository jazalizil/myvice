package com.vice;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

class Utils
{
	Context _c;
	
	public Utils(Context c)
	{
		_c = c;
	}
	
	@SuppressWarnings("deprecation")
	protected void alertBox(String mymessage)
	{
		AlertDialog  win = new AlertDialog.Builder(_c).create();
		win.setMessage(mymessage);
		win.setTitle("Epijobs");
		win.setCancelable(true);
		win.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton){
				System.exit(1);
				}
		});
		win.setIcon(R.drawable.ic_launcher);
		win.show();
	}
}