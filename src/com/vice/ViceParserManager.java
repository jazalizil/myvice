package com.vice;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;


public class ViceParserManager extends AsyncTask<String, String, ArrayList<ViceItem> > {
	Context c;
//	private ProgressDialog myProgressDialog;
	
	public ViceParserManager (Context context) {
		c = context;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
//		myProgressDialog = ProgressDialog.show(c, "", "Chargement");
		Toast.makeText(c, "Téléchargement des articles", Toast.LENGTH_SHORT).show();
	}

	@Override
	protected ArrayList<ViceItem> doInBackground(String... args) {
		ViceParser vParser = new ViceParser(c);
		// Getting Vice Articles from URL
		ArrayList<ViceItem> items = vParser.getItemsFromUrl(args[0]);
		return items;
	}
	
	@Override
	protected void onPostExecute(ArrayList<ViceItem> jobs) {
		super.onPreExecute();
//		myProgressDialog.dismiss();
		Toast.makeText(c, "Téléchargement terminé !", Toast.LENGTH_SHORT).show();
	}
}

