package com.vice;

import android.graphics.Bitmap;
 

public interface ViceItem {
	enum ItemType {VIDEO, ARTICLE};
	public String getUrl();
	public String getTitle();
	public String getContent();
	public ItemType getItemType();
	public String toString();
	public boolean isOk();
	public Bitmap getImageBitmap();
}
